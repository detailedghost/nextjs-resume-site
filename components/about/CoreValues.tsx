import React from 'react'

export const CoreValues = () => {
    const values = ['Be honest, patient, and relentless in a kind way', 'Keep an open mind and keep learning', 'Work hard, sleep hard', 'Live artfully'];
    return (
        <div className="core-values">
            <h3>Core Values</h3>
            <ul> {values.map((v, i) => <li key={i}>{v}</li> )} </ul>
        </div>
    )
}
