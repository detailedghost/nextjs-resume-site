import React from 'react'

export const Credits = () => {
    return (
                <div className="credits">
                    <h3 className="title credits__title">Credits</h3>
                    <ul className="credits__list">
                        <li>Site created by <a href="mailto:dante@thundermane.com">Dante Burgos</a>👻</li>
                    </ul>
                </div>
    )
}
