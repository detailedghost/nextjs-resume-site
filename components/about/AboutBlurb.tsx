import React from 'react'

export const AboutBlurb = () => {
    return (
                <div className="about-blurb">
                    <h2 className="about-blurb__title"></h2>
                    <div className="about-blurb__img" >
                        <img src="/images/profilepic.jpg" alt="blue eye auburn hair girl smiling into camera"></img>
                    </div>
                    <div className="about-blurb__text">
                        <p>
                            I love the challenge 
                            of finding creative 
                            solutions in work and 
                            in play.
                        </p>
                        <p>
                            When I'm not working, I enjoy 
                            living in an artful way. My art 
                            passions include painting, 
                            drawing, photography, 
                            sculpture, sewing, and 
                            cooking.
                        </p>
                        <p style={{display: 'none'}}>
                            These are a few of my favorite 
                            travel photographs.
                        </p>
                    </div>
                </div>
    )
}
