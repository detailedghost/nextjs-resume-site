
import React from 'react'

export const Gallery = () => {
    const Images = () => {
        let result = Array.from(Array(7).keys())
            .map(i => <div key={i} style={ { backgroundImage: `url(/images/a_${i}.jpg)`} } className="about-gallery__picture"></div>);
        result.shift();
        return result;
    }

    return (
        <div className="about-gallery">
            {Images()}
        </div>
    )
}
