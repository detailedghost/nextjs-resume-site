import React from 'react'
import PictureFrame from '../util/PictureFrame';

export const Quotes = () => {
    const quotes: string[] = ['Whether you think you can, or you think you can\'t - you\'re right.', 
        "The extra mile is never crowded",
        'Show up, do the work, look on the bright side',
    ];

    return (
        <div className="quotes">
            <h3>Quotes I live by</h3>
            <ul>
                {quotes.map((m, i) => <li key={i}>{m}</li>)}
            </ul>
        </div>
    )
}
