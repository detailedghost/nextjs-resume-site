import React, { useState, useEffect } from "react";

const IntroHeader: React.FC = () => {
    const messages = [
        "experienced",
        "reliable",
        "smart",
        "artistic",
        "hard working",
        "your next big hire",
        "Leila Salera"
    ];

    const [currentMsg, setMsg] = useState(0);

    useEffect(() => {
        let ticker = setInterval(() => {
                if(currentMsg + 1 < messages.length){
                    setMsg(currentMsg + 1);
                } 
            }, 700);

        return () => {
            clearInterval(ticker);
        };
    }, [currentMsg]);


    return (
        <header className="intro" id="home">
            <div className="intro__backdrop"></div>
            <span className="title intro__message">
                {messages[currentMsg]}
            </span>
        </header>
    );
}

export default IntroHeader;