import React, { FC, useState, useCallback, useEffect } from "react";
import Head from "next/head";
import settings from '../../appsettings.json';
import Navbar from "../navbar/Navbar";
import ScrollToTop from '../util/ScrollToTop';
import "../../styles/fontawesome";

const Layout: FC = (props) => {
    const seokeywords = [
        "leila",
        "salera",
        "ui",
        "user interface",
        "ux",
        "user experience",
        "dallas",
        "texas",
        "tx",
    ];

    const [loc, setLoc] = useState(-1);

    const updatePosition = useCallback((e: any) => {
        setLoc(window.scrollY);
    }, [setLoc]);

    useEffect(() => {
        window.addEventListener("scroll", updatePosition);
        return () => {
            window.removeEventListener("scroll", updatePosition);
        };
    }, [loc])

    return (
    <div>
        <Head>
            <title>{settings.title || "My Resume Site"}</title>
            <meta name="description" content="Get to know your next best hire: Leila Salera!"></meta>
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <meta name="keywords" content={seokeywords.reduce((prev, cur) => `${prev},${cur}`)}></meta>
        </Head>
        <div className="universe">
            <Navbar active={true}></Navbar>
            <div className="body-content">
                {props.children}
            </div>
            <ScrollToTop active={loc > 0}></ScrollToTop>
        </div>
    </div>
    );
}

export default Layout;