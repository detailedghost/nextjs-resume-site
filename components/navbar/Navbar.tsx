import React, {useState, useLayoutEffect} from "react";
import style from "./Navbar.module.css";
import Link from 'next/link'

export interface NavbarProps { 
    active: boolean;
}

const Navbar: React.FC<NavbarProps> = (props: NavbarProps) => {
    const styles = props.active ? style.navbar : `${style.navbar} ${style["navbar--hide"]}`;

    return (
        <nav className={styles}>
            <ul>
                <li>
                    <Link href="/">
                        <a title="Home">
                            <span className={style.linkTitle}>Home</span>
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="experience">
                        <a title="Experience">
                            <span className={style.linkTitle}>Experience</span>
                        </a>
                    </Link>
                </li>
                <li>
                    <Link href="about">
                        <a title="About" href="#about">
                            <span className={style.linkTitle}>About</span>
                        </a>
                    </Link>
                </li>
                <li>
                    <a title="Connect" href="mailto:leilasalera@gmail.com">
                        <span className={style.linkTitle}>Connect</span>
                    </a>
                </li>
            </ul>
        </nav>
    );
}

export default Navbar;