import React from 'react'
import { ImageMediaFile } from '../../models/MediaFile';


export default function MediaFileImage(props: { file: ImageMediaFile }) {

    return (
        <a href={props.file.location} className="media-file-image">
            <img src={props.file.location}></img>
        </a>
    );
}
