import React from 'react';

import moment from 'moment'

export interface DateLengthProps {
    startDate: Date;
    endDate?: Date;
    format?: string;
    className?: string;
}

export default function DateLength(props: DateLengthProps){
    const start = moment(props.startDate);
    const formatString = props.format || 'MMMM YYYY';
    const startPrint = start.format(formatString);

    let finalPrint = `${startPrint} - Today`;

    if(props.endDate){
        const end = moment(props.endDate);

        if(end.diff(start) === 0) finalPrint = startPrint;
        else finalPrint = `${startPrint} - ${end.format(formatString)}`;
    } 

    return <span className={props.className}>{finalPrint}</span>;
}