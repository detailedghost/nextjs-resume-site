import React, { useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";

export interface PictureFrameProps {
    imgSrc?: string;
    children?: any
}

export default function PictureFrame(props: PictureFrameProps) {
    let styles = {};
    if(props.imgSrc) {
        styles = { 
            backgroundImage: `url(${props.imgSrc})` 
        };
    }

    const [tgl, setTgl] = useState(false);

    let style = "picture-frame";
    if(tgl) style += " active";


    return (
        <div className={style} style={styles}>
            {props.children}
            <button className="picture-frame__toggle-btn" title="Toggle Job Description">
                <FontAwesomeIcon icon={tgl ? faMinus : faPlus} onClick={() => setTgl(!tgl)}></FontAwesomeIcon>
            </button>
        </div>
    );
}