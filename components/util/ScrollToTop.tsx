import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp } from '@fortawesome/free-solid-svg-icons'

interface ScrollToTopProps {
    active?: boolean;
}

export default function ScrollToTop(props: ScrollToTopProps) {
    function backToTop() {
        window.scrollTo(0, 0);
    }

    let styleName = "top-scroll-btn";
    if(props.active) styleName += " show"

    return (
        <button className={styleName} onClick={() => backToTop()} title="Go back to top of page">
            <FontAwesomeIcon icon={faArrowUp}></FontAwesomeIcon>
            <span className="top-scroll-btn__label">back to top</span>
        </button>
    );
}