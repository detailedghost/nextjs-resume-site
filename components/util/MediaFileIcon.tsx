import React from 'react'
import { MediaFile } from '../../models/MediaFile';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


export default function MediaFileIcon(props: { file: MediaFile }) {

    return (
        <div className="media-file">
            <a href={props.file.location} className="media-file__link">
                <FontAwesomeIcon icon={props.file.iconType} className="media-file__icon"></FontAwesomeIcon>
                <span className="media-file__text">{props.file.name}</span>
            </a>
        </div>
    );
}
