import React from 'react'

export interface ResumeItemUrlProps{
    title: string;
    url?: string;
    className?: string;
}

export default function ResumeItemUrlProps(props: ResumeItemUrlProps) {
    return props.url ? 
        <span className={props.className}><a href={props.url}>{props.title}</a></span> :
        <span className={props.className}>{props.title}</span>;
}
