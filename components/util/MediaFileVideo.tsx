import React from 'react'
import { ImageMediaFile } from '../../models/MediaFile';


export default function MediaFileVideo(props: { file: ImageMediaFile }) {

    return (
        <div className="media-file-image">
            <iframe width="560" height="315" src={props.file.location} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
    );
}
