import React from 'react';
import { Skill, SkillLevel } from '../../models/Education';

export interface SkillItemProps {
    skill: Skill;
}

export default function SkillItem(props: SkillItemProps) {
    let itemLevel = "";
    switch(props.skill.level){
        case SkillLevel.Beg: 
            itemLevel = "skill-item--beg";
            break;

        case SkillLevel.Inter:
            itemLevel = "skill-item--inter";
            break;

        case SkillLevel.Pro:
            itemLevel = "skill-item--pro";
            break;

        default:
            itemLevel = "";
            break;
    }

    return (
        <div className={`skill-item ${itemLevel}`}>
            <span className="skill-item__vocation">{props.skill.vocation}</span> <span className="skill-item__level"> - {props.skill.level}</span>
        </div>
    )
};
