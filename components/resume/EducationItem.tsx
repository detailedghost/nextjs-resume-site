import React from 'react'
import { Education } from '../../models/Education'
import DateLength from '../util/DateLength'

export interface EducationItemProps {
    education: Education;
}

export default function EducationItem(props: EducationItemProps) {
    const majorDegreeList = () => props.education.majorDegree ? 
        props.education.majorDegree.map((d,i) => <li key={i}>{d}</li>) : undefined;
    const minorDegreeList = () => props.education.minorDegree ? 
        props.education.minorDegree.map((d,i) => <li key={i}>{d}</li>) : undefined;
    const activitiesList = () => props.education.activities ?
        props.education.activities.map((a,i) => <li key={i}>{a}</li>) : undefined;
    const awardList = () => props.education.awards ? 
        props.education.awards.map((a,i) => <li key={i}>{a}</li>) : undefined;

    return (
        <div className="education-item">
            <h2 className="education-item__school">{props.education.school}</h2>
            <DateLength startDate={props.education.startDate} endDate={props.education.endDate} className="education-item__timeframe"></DateLength>
            <div className="education-item__major-degree">
                <ul className="education-item__major-degree__list">{majorDegreeList()}</ul>
            </div>
            <div className="education-item__minor-degree">
                <ul className="education-item__minor-degree__list">{minorDegreeList()}</ul>
            </div>
            <div className="education-item__activities">
                <ul className="education-item__activities__list">{activitiesList()}</ul>
            </div>
            <div className="education-item__awards">
                <ul className="education-item__activities__list">{awardList()}</ul>
            </div>
            <p className="education-item__misc">{props.education.misc}</p>
        </div>
    )
}
