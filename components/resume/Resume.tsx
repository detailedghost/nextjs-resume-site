import React, { useState, useEffect } from "react";
import ResumeSection from "./ResumeSection";
import { SkillList } from "./SkillList"

import { EducationList } from "./EducationList";
import { JobList } from "./JobList";
import { ProjectList } from "./ProjectList";
import { CertificationList } from "./CertificationList";

export default function Resume() {

    return (
        <div className="resume">
            <ResumeSection title="Experience" className="resume-jobs">
                <JobList />
            </ResumeSection>   
            <ResumeSection title="Volunteering" className="resume-volunteer">
                <ProjectList type="v" />
            </ResumeSection>
            <ResumeSection title="Projects" className="resume-project">
                <ProjectList />
            </ResumeSection>
            <ResumeSection title="Education" className="resume-education resume-section--split" >
                <div className="resume-degrees">
                    <EducationList />
                </div>
                <div className="resume-certifications">
                    <CertificationList />
                </div>
            </ResumeSection>
            <ResumeSection title="Skills" className="resume-vocation resume-section--split">
                <SkillList></SkillList>
            </ResumeSection>
            <ResumeSection className="resume-download">
                <a href="https://www.dropbox.com/sh/tvn0qxl26oxt9ne/AAC4OQRbQoa0IIxU99CzwAbaa?dl=0&preview=Leila_Salera_Resume.pdf">
                    Download Resume
                </a>
            </ResumeSection>
        </div>
        );
    }