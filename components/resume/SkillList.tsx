import React from 'react'
import { Skill, SkillLevel } from '../../models/Education'
import SkillItem from './SkillItem'
import axios from "axios";
import skillDB from '../../db/SkillDB'

export const SkillList = () => {
    const [skills, setSkill] = React.useState(skillDB);

    // React.useEffect(() => {
    //     if(skills.length < 1) axios.get("/api/skill").then(res => setSkill(res.data));
    // }, [])

    const pro = skills
        .filter(s => s.level === SkillLevel.Pro)
        .map((s,i) => <SkillItem key={i} skill={s}></SkillItem>);
    const inter = skills
        .filter(s => s.level === SkillLevel.Inter)
        .map((s, i) => <SkillItem key={i} skill={s}></SkillItem>);
    const beg = skills
        .filter(s => s.level === SkillLevel.Beg)
        .map((s, i) => <SkillItem key={i} skill={s}></SkillItem>);

    return (
        <>
            <div className="skill-list">
                <h2 className="skill-list__title">Proficient</h2>
                <div className="skill-list__list">
                    {pro}
                </div>
            </div>
            <div className="skill-list">
                <h2 className="skill-list__title">Intermediate</h2>
                <div className="skill-list__list">
                    {inter}
                </div>
            </div>
            <div className="skill-list">
                <h2 className="skill-list__title">Beginner</h2>
                <div className="skill-list__list">
                    {beg}
                </div>
            </div>
        </>
    );
}
