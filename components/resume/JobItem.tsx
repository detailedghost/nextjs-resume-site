import React, { useState } from "react";
import Job from "../../models/Jobs";

import DateLength from "../util/DateLength";
import ResumeItemUrl from '../util/ResumeItemUrl'
import { MediaFile, MediaFileType } from "../../models/MediaFile";
import MediaFileImage from "../util/MediaFileImage";
import MediaFileIcon from "../util/MediaFileIcon";
import MediaFileVideo from "../util/MediaFileVideo";


export interface ResumeItemProps {
    job: Job;
}

export default function ResumeItem(props: ResumeItemProps) {

    const mapResponsbilities = () => {
        if(!props.job.responsibilities) return;
        return props.job.responsibilities
            .map(x => <li key={x.substr(0, 10).toString()}>{x}</li>);
    };

    const mapFiles = (m: MediaFile, i) => {
        switch(m.iconType){
            case MediaFileType.Image:
                return <MediaFileImage key={i} file={m}></MediaFileImage> 

            default: 
                return <MediaFileIcon key={i} file={m}></MediaFileIcon> 
        }
    }

    const mediaFiles = () => {
        if(!props.job.media) return undefined;
        else return props.job.media.map(mapFiles);
    }

    let jobItemClass = "job-item";

    return (
        <div className={jobItemClass}>
            <h2 className="job-item__company">
                <ResumeItemUrl title={props.job.company} url={props.job.companyUrl}></ResumeItemUrl>
            </h2>
            <h3 className="job-item__title">{props.job.jobTitle}</h3>
            <h4 className="job-item__timeframe">
                <DateLength startDate={props.job.startDate} endDate={props.job.endDate}></DateLength>
            </h4>
            <p className="job-item__description">{props.job.description}</p>
            <ul className="job-item__responsibilities">
                {mapResponsbilities()}
            </ul>
            <ul className="job-item__media">
                {mediaFiles()}
            </ul>
        </div>
    );
}