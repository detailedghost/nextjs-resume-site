import React from 'react'
import axios from 'axios'
import { Certification } from '../../models/Education';
import CertificationItem from './CertificationItem';
import certDB from '../../db/CertificationDB';

export const CertificationList = () => {
    const [certs, setCerts] = React.useState(certDB);
    // React.useEffect(() => {
    //     let getItems = () => axios.get('/api/certification').then(res => setCerts(res.data));
    // }, []);
    return (
        <>
            {certs.map(c => <CertificationItem key={c.credentialId} cert={c}></CertificationItem>)}
        </>
    )
}
