import React from 'react'
import { Project } from '../../models/Project';
import DateLength from '../util/DateLength';
import MediaFileIcon from '../util/MediaFileIcon';
import { MediaFile, ImageMediaFile, MediaFileType } from '../../models/MediaFile';
import MediaFileImage from '../util/MediaFileImage';

export interface ProjectItemProps {
    key: string;
    project: Project;
}

export default function ProjectItem(props: ProjectItemProps){
    const mapFiles = (m: MediaFile, i) => {
        switch(m.iconType){
            case MediaFileType.Image:
                return <MediaFileImage key={i} file={m}></MediaFileImage> 

            default: 
                return <MediaFileIcon key={i} file={m}></MediaFileIcon> 
        }
    }
    const mediaFiles = () => {
        if(!props.project.media) return undefined;
        else return props.project.media.map(mapFiles);
    }
    
    return (
        <div className="project-item">
            <h2 className="project-item__name">{props.project.name}</h2>
            <DateLength startDate={props.project.startDate} endDate={props.project.endDate} className="project-item__timeframe"></DateLength>
            <p className="project-item__description">{props.project.description}</p>
            <div className="project-item__media">
                {mediaFiles()}
            </div>
        </div>
    );
};