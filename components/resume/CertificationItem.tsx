import React from 'react'
import { Certification } from '../../models/Education'
import DateLength from '../util/DateLength'
import ResumeItemUrl from '../util/ResumeItemUrl';

export interface CertificationItemProps {
    cert: Certification;
}

export default function CertificationItem(props: CertificationItemProps) {
    return (
        <div className="certificate-item">
            <span className="certificate-item__name">{props.cert.name}</span>
            <ResumeItemUrl title={props.cert.org} url={props.cert.url} className="certificate-item__org"></ResumeItemUrl>
            <DateLength startDate={props.cert.startDate} endDate={props.cert.endDate} className="certificate-item__timeframe"></DateLength>
        </div>
    )
}
