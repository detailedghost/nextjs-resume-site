import React, { Dispatch, SetStateAction } from 'react'
import axios from "axios";
import { Project } from '../../models/Project';
import PictureFrame from '../util/PictureFrame';
import ProjectItem from './ProjectItem';
import { Volunteering, Projects } from '../../db/ProjectDB'

export const ProjectList = (props) => {
    const [proj, setProj] = React.useState(props.type === 'v' ? Volunteering : Projects);
    // React.useEffect(() => {
    //     if(props.type === "v") axios.get("/api/project?type=V").then(res => setProj(res.data));
    //     else axios.get('/api/project').then(res => setProj(res.data));
    // }, []);
    return (
        <>
        {proj.map((p,i) => {
            return (
                <PictureFrame key={i} imgSrc={`/images/pf_${props.type || 'p'}_${i+1}.jpeg`}>
                    <ProjectItem key={i.toString()} project={p}></ProjectItem>
                </PictureFrame>
            );
        })}
        </>
    )
}
