import React from 'react'
import Job from '../../models/Jobs';
import axios from 'axios';
import PictureFrame from '../util/PictureFrame';
import JobItem from './JobItem';
import jobDB from '../../db/JobDB'

export const JobList = () => {
    const [jobs, setJobs] = React.useState(jobDB);
    // React.useEffect(() => { 
    //     if(jobs.length < 1) axios.get("/api/job").then(res => setJobs(res.data));
    // }, []);
    return (
        <>
        {jobs.map((j,i) => {
            return (
                <PictureFrame key={i} imgSrc={`/images/pf_j_${i+1}.jpeg`}>
                    <JobItem key={i} job={j} />
                </PictureFrame>
            );
        })}
        </>
    )
}
