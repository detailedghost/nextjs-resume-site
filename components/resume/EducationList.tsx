import React from 'react'
import { Education } from '../../models/Education';
import EducationItem from './EducationItem';
import axios from 'axios';
import educationDB from '../../db/EducationDB'

export const EducationList = () => {
    const [eds, setEd] = React.useState(educationDB);
    // React.useEffect(() => {
    //     if(eds.length < 1) axios.get("/api/education").then(res => setEd(res.data));
    // }, [])
    return (
        <>
            {eds.map((e,i) => <EducationItem key={i} education={e}></EducationItem>)}
        </>
    )
}
