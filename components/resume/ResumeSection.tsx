import React from "react";

export interface ResumeSectionProps {
    title?: string;
    className?: string;
    children: any;
}

const ResumeSection: React.FC<ResumeSectionProps> = (props: ResumeSectionProps) => {
    return (
        <div className={`resume-section ${props.className}`}>
            {props.title ? <h2 className="resume-section__title">{props.title}</h2> : <></>}
            <div className="resume-section__container">
                {props.children}
            </div>
        </div>
    );
}
export default ResumeSection;