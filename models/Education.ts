export class Education {
    constructor(
         public school: string,
         public startDate: Date,
         public endDate?: Date,
         public majorDegree?: string[],
         public minorDegree?: string[],
         public activities?: string[],
         public awards?: string[], 
         public gpa?: number,
         public misc?: string
    ){}
}

export class Certification {
    constructor(
        public name: string,
        public org: string,
        public credentialId: string,
        public startDate: Date,
        public endDate?: Date,
        public url?: string
    ){}
}

export enum SkillLevel {
    Beg = "Beginner",
    Inter = "Intermediate",
    Pro = "Proficient"
}

export class Skill {
    constructor(
        public vocation: string,
        public level: SkillLevel
    ) {}
}