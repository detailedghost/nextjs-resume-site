import { MediaFile } from "./MediaFile";

export class Project {
    constructor(
        public name: string, 
        public description: string, 
        public startDate: Date, 
        public endDate?: Date,
        public media?: MediaFile[]
    ) { }
}
