import { Project } from "./Project";
import { MediaFile } from "./MediaFile";

export default class Job {
    constructor(
        public jobTitle: string,
        public company: string,
        public description: string,
        public startDate: Date,
        public location: string,
        public endDate?: Date,
        public responsibilities?: string[],
        public companyUrl?: string,
        public projects?: Project[],
        public media?: MediaFile[]
    ){}
};