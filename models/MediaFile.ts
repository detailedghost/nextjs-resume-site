export enum MediaFileType {
    File = "file",
    MSWord = "file-word",
    MSExcel = "file-excel",
    MSPPT = "file-powerpoint",
    PDF = "file-pdf",
    Image = "image",
    Video = "video"
}


export class MediaFile {
    constructor(
        public location: string,
        public name: string = "File",
        public iconType: MediaFileType = MediaFileType.File,
        public showName: boolean = false
    ){}
}

export class ImageMediaFile extends MediaFile {
    constructor(
        public location,
        public name: string = "Image",
    ) {
        super(name, location, MediaFileType.Image);
    }
}

export class VideoMediaFile extends MediaFile {
    constructor(
        public location,
        public name: string = "Video",
    ) {
        super(name, location, MediaFileType.Video);
    }
}