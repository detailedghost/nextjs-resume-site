# NextJS Resume Site
## Introduction
This is a simple resume site that plays around with the NextJS framework. If you'd like to customize it to your own liking, simple drop in logic into the `db` folder, and swap out the images. 

Images follow the following format:
`pf_[COMPONENT]_1.jpeg` = [COMPONENT] meaning 'J' for job, 'P' for project, and 'V' for volunteering.

Have fun! 👻