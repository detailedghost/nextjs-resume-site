import { library } from '@fortawesome/fontawesome-svg-core';
import { 
    faHome,
    faFile,
    faFileExcel, 
    faFilePowerpoint, 
    faFilePdf, 
    faFileWord,
    faPlus,
    faMinus,
    faArrowUp,
    faImage,
    faVideo
 } from '@fortawesome/free-solid-svg-icons';


library.add(
    faHome,
    faFile,
    faFileExcel, 
    faFilePowerpoint, 
    faFilePdf, 
    faFileWord ,
    faPlus,
    faMinus,
    faArrowUp,
    faImage,
    faVideo
);