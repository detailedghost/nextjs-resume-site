import Layout from "../components/layout";
import Resume from "../components/resume/Resume";


const Experience = () => {
    return (
        <Layout>
            <div className="navbar-space">
                <Resume></Resume>
            </div>
        </Layout>
    );
}

export default Experience;