import Layout from "../components/layout";
import IntroHeader from '../components/intro/IntroHeader';

const Home = () => {
    return (
    <Layout>
        <IntroHeader></IntroHeader>
    </Layout>
    );
}

export default Home;