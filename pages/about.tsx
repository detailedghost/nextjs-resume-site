import Layout from "../components/layout"
import { AboutBlurb } from "../components/about/AboutBlurb";
import { Credits } from "../components/about/Credits";
import { Quotes } from "../components/about/Quotes";
import { CoreValues } from "../components/about/CoreValues";
import { Gallery } from "../components/about/Gallery";

const About = () => {
    return (
        <Layout>
            <div className="navbar-space center-content">
                <AboutBlurb />
            </div>
            <div className="about-content" >
                <Gallery />
                <Quotes />
                <CoreValues />
                <Credits />
            </div>
        </Layout>
    )
};

export default About;