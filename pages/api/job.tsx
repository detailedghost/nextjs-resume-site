import jobs from '../../db/JobDB'

export default function(req, res){
    if(req.method === "GET") res.status(200).json(jobs);
    else res.status(401);
}