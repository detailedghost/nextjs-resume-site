import { Projects, Volunteering } from '../../db/ProjectDB';

export default function(req, res){
    if(req.method === "GET"){
        if(req.query["type"] === 'V')
            res.status(200).json(Volunteering);
        else
            res.status(200).json(Projects);

    } 
    else res.status(401);
}