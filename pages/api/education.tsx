import edb from '../../db/EducationDB';

export default (req, res) => {
    if(req.method === 'GET')
        res.status(200).json(edb);
    else
        res.status(400);
};