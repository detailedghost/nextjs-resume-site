import skills from '../../db/SkillDB';

export default function(req, res){
    if(req.method === "GET") res.status(200).json(skills);
    else res.status(401);
}