import Job from "../models/Jobs";
import { MediaFile, MediaFileType, VideoMediaFile } from "../models/MediaFile";

const JobDB: Job[] = [
    {
        jobTitle: "Product Owner / Founder",
        company: "Industry Contact Data",
        companyUrl: "https://industrycontactdata.com",
        startDate: new Date("7/1/2018"),
        endDate: new Date("1/1/2020"),
        location: "Dallas, TX",
        description: "Through an industry-exclusive opt-in approach, commercial real estate firms can use ICD's up-to-date contact database for their individual needs. Users can create, organize, and download custom contact data lists.",
        responsibilities: [
            "PO: road maps, product vision, UI/UX, project management, backlog management and user story refinement. Headed 3 software development teams.",
            "Used Kanban agile project management. Conducted beta testing and documented results. Built custom website with WordPress. Designed web app and mobile friendly version.",
            "Founder: business strategies, business case, pitch deck, pro forma financial statements, lead generation, market and competition research.",
            "Planned and executed marketing campaigns. Direct email campaigns resulted in 5-15% response rate and 30-40% sign up conversion rates. Generated new business through calls, presentations, and networking. Onboarded on average 7 new clients per month.",
        ],
        media: [
            new VideoMediaFile("https://youtu.be/0onkYDHDeZE", "ICD Introduction Video")
        ]
    },
    {
        jobTitle: "Marketing & Research Anaylst", 
        company: "Falcon Realty Advisors",
        startDate: new Date("9/1/2017"),
        endDate: new Date("2/1/2019"),
        location: "Dallas, TX",
        description: "At Falcon, we’re always in pursuit of the ideal match. We exclusively work with retail, restaurant and entertainment tenants in real estate strategy, construction and development, marrying the big picture with the bottom line through a model of conscious business.",
        responsibilities: [
            "Conducted research and managed projects, tours, and reports for brokers and their clients.",
            "Executed due diligence reports for real estate development projects. Drafted and edited Letters of Intents, Leasing Agreements, and Commission Agreements.",
            "Created instruction manuals, trained 5 new team members on internal processes (demographics, ownership, prospects, tour books), technology (LandVision, Magnify, STDB), and industry terminology.",
            "Designed marketing collateral and customized pitch books for clients and brokers. Formulated eblasts and managed Mailchimp scheduling."
        ],
    }, 
    {
        jobTitle: "Product and Business Development Consultant",
        company: "Self-Employed",
        startDate: new Date("7/1/2014"),
        endDate: new Date("9/1/2017"),
        location: "San Francisco, CA",
        description: "Consulted in the early development stages of products and business ventures.",
        responsibilities: [
            "Consulted for Birr Castle Group, Ltd. and a cannabis dispensary start-up in San Francisco. Past projects include: medical devices, perfumes, online consignment, real-estate, children toys, food flavoring, and alternative energy.",
            "For each project, analyzed market size, competition, trends & short-listed potential channel partners. Contributed to creating new branding, product differentiation, packaging options and product launch materials.",
        ],
    },
    {
        jobTitle: "Part-time Assistant Manager",
        company: "Birr Castle Group, Ltd.",
        startDate: new Date("4/1/2015"),
        endDate: new Date("11/1/2017"),
        location: "Ireland & San Francisco, CA",
        description: "Support a Birr Castle Board Member (Jonathan Eisenberg) and the Birr Castle Foundation in the project/product development.",
        responsibilities: [
            "Funding processes for a $40M real estate project, a $10M country music festival (2018), $1M new whiskey company and $250,000 Birr Castle Naturals lotion line.",
            "For each Birr Castle effort, researched markets trends, target market, competition and completed a SWOT analysis (strengths, weaknesses, opportunities, threats). Assist in preparation for meetings with potential business partners.",
            "For the Birr Castle real estate and music festival projects, identified a range of US and European strategic and financial investors including hedge funds, venture capital groups and potential industry partners. In discussion with the Birr Castle board member, short listed the “good fit” investors and partners. Results have allowed Birr Castle to increase funding and create partnerships.",
        ],
        media: [
            new MediaFile("/files/birrmarketing.pdf", "Birr Marketing Plan")
        ]
    },
    {
        jobTitle: "Sales Agent Team Member",
        company: "David Wong State Farm",
        startDate: new Date("9/1/2016"),
        endDate: new Date("6/1/2017"),
        location: "Mill Valley, CA",
        description: "Sold and maintained personal insurance plans ranging from Home and Auto to Umbrella and Flood insurance plans.",
        responsibilities: [
            "Answered phones and emails, reminded clients about upcoming payments or changes, took payments, fixed errors, provided forms, wrote quotes and insurance policies by using ABS and NECHO to retain and grow clientele. Wrote and made changes to policies with up to $5 million coverage.",
            "Used a customer-focused, needs-based review process to educate customers about insurance options.",
            "Trained new team members with State Farm computer programs, answering phones, selling products, and customer service. Created training and instruction manual for NFIP insurance.",
        ],
        media: [
            new MediaFile( "/files/floodtraining.pdf", "Training Manual", MediaFileType.PDF)
        ]
    },
    {
        jobTitle: "Research Intern",
        company: "Global Energy Innovations",
        startDate: new Date("6/1/2014"),
        endDate: new Date("8/1/2014"),
        location: "Santa Clara",
        description: "",
        responsibilities: [
            "Researched market size, industry competition, company finances, and stock market price history for potential business partners.",
            "Analyzed freight and carrier company services to find a cost effective and efficient delivery method.",
            "Updated inventory in MISys, checked bill of materials, created and filed purchase orders, entered bills into QuickBooks, filed credit card receipts, mailed items, and contacted suppliers.",
            "Communicated directly with the Department of Defense about contract orders and inspections.",
            "Managed the military shipping process, used Mil-Pac software, completed Wide Area Workflow documents for DoD contract orders, prepared orders for inspector and developed process instructions.",
        ],
    },
];

export default JobDB;