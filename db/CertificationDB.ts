import { Certification } from "../models/Education";

export default [
    new Certification("Certified Scrum Product Owner (CSPO)", "Scrum Alliance", "1033863", new Date("1/1/2020"), new Date("1/1/2022"), "https://bcert.me/bc/html/show-badge.html?b=gavnzeuv"),
    new Certification("California Property and Casualty Insurance License", "California Department of Insurance", "0L04833", new Date("8/1/2016"), new Date("8/1/2018"), "https://interactive.web.insurance.ca.gov/eLicense/license?event=eLicensePdf&id=1636569")
];