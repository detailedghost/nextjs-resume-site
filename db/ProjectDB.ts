import { Project } from "../models/Project";
import { MediaFile, MediaFileType, ImageMediaFile } from "../models/MediaFile";

export const Projects: Project[] = [
    new Project("Econometrics Research", "Econometrics was one of my favorite courses at Occidental College. My final Econometrics report investigated what might cause the variation of housing values in California. It focused on how population, median household income, unemployment rate, recent job growth and property tax rate influence the median house and condominium value in Californian cities.", new Date("5/1/2015"), new Date("5/1/2015"), 
        [
            new MediaFile( "/files/econpaper.pdf", "Report", MediaFileType.PDF)
        ]),
    new Project("Noreen Designs", "An online handmade jewlery business", new Date("5/1/2015"), new Date("11/1/2015"),
        [
            new ImageMediaFile("/images/p_1.jpg"),
            new ImageMediaFile("/images/p_2.jpg")
        ]),
];

export const Volunteering: Project[] = [
    new Project("Phillipines Dental Volunteering", "Assisted in dental cleaning, activities, cooking for the patients, families and neighborhood. Worked with Larry Mally, founder of Medical Mission Matters, to promote the non-profit in the United States. Co-created a promotional video.", 
    new Date("6/1/2017"),
    undefined,
    [
        new ImageMediaFile("/images/v_1.jpg"),
        new ImageMediaFile("/images/v_2.jpg"),
        new ImageMediaFile("/images/v_3.jpg"),
        new ImageMediaFile("/images/v_4.jpg")
    ])
];


export default Projects;