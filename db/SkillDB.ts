import { Skill, SkillLevel } from "../models/Education";

const SkillsDB: Skill[] = [
    new Skill("Adobe InDesign", SkillLevel.Pro),
    new Skill("Trello", SkillLevel.Pro),
    new Skill("Microsoft Excel", SkillLevel.Pro),
    new Skill("Microsoft Word", SkillLevel.Pro),
    new Skill("Microsoft Powerpoint", SkillLevel.Pro),
    new Skill("Microsoft Outlook", SkillLevel.Pro),
    new Skill("Business Strategy", SkillLevel.Pro),
    new Skill("Onboarding/Training New Hires", SkillLevel.Pro),
    new Skill("Commercial Real Estate Operations", SkillLevel.Pro),
    new Skill("Personal Insurance Sales", SkillLevel.Pro),

    new Skill("Agile - Scrum", SkillLevel.Inter),
    new Skill("Agile - Kanban", SkillLevel.Inter),
    new Skill("Product Roadmaps", SkillLevel.Inter),
    new Skill("Product Backlog Refinement", SkillLevel.Inter),
    new Skill("Sprint Planning", SkillLevel.Inter),
    new Skill("User Story Mapping", SkillLevel.Inter),
    new Skill("Beta Testing / UAT", SkillLevel.Inter),
    new Skill("UI/UX Design", SkillLevel.Inter),
    new Skill("Marketing Management", SkillLevel.Inter),
    new Skill("SWOT Analysis", SkillLevel.Inter),
    new Skill("WordPress", SkillLevel.Inter),
    new Skill("Mailchimp", SkillLevel.Inter),

    new Skill("SQL", SkillLevel.Beg),
    new Skill("STATA", SkillLevel.Beg),
    new Skill("Tableau", SkillLevel.Beg),
    new Skill("Gitlab", SkillLevel.Beg),
    new Skill("Microsoft Azure", SkillLevel.Beg),
    new Skill("Azure Data Studio", SkillLevel.Beg),
    new Skill("Sketch", SkillLevel.Beg),
    new Skill("American Sign Language", SkillLevel.Beg),
];

export default SkillsDB;