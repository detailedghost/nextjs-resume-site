import { Education } from "../models/Education";

const EducationDB: Education[] = [
    new Education("Occidental College", 
        new Date("8/1/2014"), new Date("5/1/2016"),
        ["Bachelors of Arts in Economics (Cum Laude)"], 
        ["Studio Art"],
        undefined, //["Oxypreneurship", "AdVantage Fundraiser - Homeless Awareness"],
        undefined,
        undefined,
        "I attended San Jose State University from August \
        2012 to May 2014 before transferring to Occidental \
        College."
    )
];

export default EducationDB;
